#!/bin/bash

#SBATCH --output=./data/%j.out
#SBATCH --error=./data/%j.err

#SBATCH --mail-type=BEGIN
#SBATCH --mail-user=ppawlowsky@ethz.ch

#SBATCH --time=0:01:00
#SBATCH --ntasks=6
#SBATCH --mem-per-cpu=20G
#SBATCH --dependency=afterany:16899189

# lscpu;
# python3 ./generatemdp/main.py -n 10000 -m 100 -t 0.1 -c uniform;
mkdir ./data/${SLURM_JOB_ID}
mpirun --report-bindings ./build/main -log_view :${SLURM_JOB_ID}log.txt; # -info info.txt -mat_view ::ascii_info;

# specs from https://scicomp.ethz.ch/wiki/Euler#Euler_VIII
# per node:
# two 64 core cpu
# 512 GB RAM
# 920,618.0 MB Scratch

# student accounts can only use 128 GiB of RAM
# Maximum: 1024 * 128 / ntasks
