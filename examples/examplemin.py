import madupite as md

# every function call should be within the PETScContextManager
with md.PETScContextManager():
    # Since we use mpirun, every line of code will be executed by each rank.
    # Parts that should only be run by a single rank must be called inside
    # an if md.MPI_master() statement.
    if md.MPI_master():
        # this function creates a random mdp with 10 states, 8 actions,
        # sparsity 0.5, and uniformly distributed stagecost
        transprob, stagecost = md.generateArtificialMDP(8, 4, 0.5)
        # write the numpy/scipy matrices to PETSc binary format
        md.writePETScBinary(stagecost, "./stagecost.bin")
        md.writePETScBinary(transprob, "./transprob.bin")

    # load the transition probabilities and stage costs
    mdp = md.PyMdp("./transprob.bin", "./stagecost.bin")
    mdp.solve("./test/", "./options.json")
