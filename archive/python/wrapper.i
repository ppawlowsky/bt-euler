%module madupite

%{
#define SWIG_FILE_WITH_INIT

#include "./mdp.hpp"
PetscErrorCode PetscInitialize(int *argc, char ***args, const char file[], const char help[]);
PetscErrorCode PetscFinalize();
%}
%include "./mdp.hpp"
typedef int PetscErrorCode;
typedef float PetscReal;
typedef PetscReal PetscScalar;
PetscErrorCode PetscFinalize();
PetscErrorCode PetscInitialize(int *argc, char ***args, const char file[], const char help[]);
