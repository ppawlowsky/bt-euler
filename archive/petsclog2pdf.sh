#!/bin/bash
# convert petsc output to pdf

enscript -r -fCourier9 $1 -o $1.ps;
ps2pdf $1.ps profile.pdf;
rm $1.ps;
