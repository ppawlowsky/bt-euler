# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import inspect

# sys.path.insert(0, os.path.abspath("../src/"))
# sys.path.insert(0, '/home/philip/miniconda3/envs/petscmadupite/lib/python3.11/site-packages')

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "madupite"
copyright = "2023, Philip Pawlowsky"
author = "Philip Pawlowsky"
release = "0.1"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# extensions = []

templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

extensions = [
    "sphinx.ext.napoleon",
    "sphinx.ext.mathjax",
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx.ext.autosummary",
    "sphinx-jsonschema",
]

highlight_language = "cython"

exclude_patterns = []

autodoc_typehints = "description"

templates_path = ["_templates"]
exclude_patterns = []
source_suffix = ".rst"
master_doc = "index"


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
html_theme_options = {
    "collapse_navigation": False,
    "navigation_depth": 2,
}


def isfunction(obj):
    return hasattr(type(obj), "__code__")


inspect.isfunction = isfunction
