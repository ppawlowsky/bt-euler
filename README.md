# Bachelor Thesis MDP

## Installation
### Python package:
After cloning the repo and navigating to the root directory you can create a conda environment for the python module
```
conda env create -f environment.yml
conda activate petscmadupite

pip install .
```
Make sure to always run your executables with mpirun:
```
mpirun -N <your#ranks> python main.py
```
You can test your installation by running an [example](./examples/).
### For C++ users:
Assuming you have a working version of PETSc, e.g. by activating the conda environment above, call the following for a build with cmake.
```
mkdir build
cd build
cmake ..
make
```
Now, you can change the main.cc file according to your needs.
Make sure to always run your executables with mpirun:
```
mpirun -N <your#ranks> ./build/main
```
Replace <your#ranks> with the number of ranks on your system

## Input data format
The data for the MDP (transition probability tensor and stage cost matrix) must be provided in the PETSc binary format. The installation with pip provides a generator for artificial MDPs and a converter from numpy/scipy matrices to PETSc binary.
```
import madupite as md
md.writePETScBinary(matrix, "path/to/matrix.bin")
```
If you want to create your own MDP you must match the data layout of this solver:
- stagecost matrix: $G_{i,j}=$ "expected cost of applying input j in state i".
- The transition probability is often expressed as a 3-dimensional tensor. In order to make use of the parallel matrix layout from PETSc, the first 2-dimensions must be combined.
$$P_{i,j,k}= \text{"transition probability from state i to k given input j"}$$
is flattened into a matrix:
$$P'_{i,j}= \text{"transition probability from state} \left\lfloor\frac{i}{\# actions}\right\rfloor \text{to state j given input } i\mod \# actions.$$
The following holds:
$$P_{i,j,k}=P'_{i*\#actions+j,k}$$
```
# combine first and second dimension of a 3d numpy array in python
1stdim, 2nddim, 3rddim = transprobtensor.shape
transprobmat = transprobtensor.reshape(1stdim * 2nddim, 3rddim)
```

## The options.json file
Specify the details of the solver. Every call of mdp.solve(...) will solve the loaded mdp according to the details set in the solver. If you want to solve the same MDP multiple times with different settings just change the options in the json file.
The main options available are:
- "initial cost type": "ones" or "random",
- "discount factor": float (0,1),
- "solver":
    - "ksptype": "richardson" or "gmres" or "bcstab",
    - "preconditioner": "jacobi" or "none,
- "policy filename": "policy.csv",
- "cost filename": "optcost.txt",
- "metadata filename": "metadata.json"

The optimal policy and optimal cost will always be computed. If you are not interested in the optimal policy for example, you can just delete the line from the json or give the empty string and no output file will be created. If you want to change the initial cost, also call mdp.generateInitCost(...) before solving.


## Using on Euler
Make sure to use the new software stack (run env2lmod in bash).
Specify the compute ressources in the launch.sh file.
```
# this loads the correct software dependencies
source moduleload.sh

mkdir build
cd build
cmake ..
make

cd ..
sbatch launch.sh
```
