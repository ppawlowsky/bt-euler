#!/bin/bash

#SBATCH --output=./data/%j.out
#SBATCH --error=./data/%j.err

#SBATCH --time=0:01:00
#SBATCH --ntasks=6
#SBATCH --mem-per-cpu=20G

mpirun --report-bindings python #######enter your executable.py##########

# specs from https://scicomp.ethz.ch/wiki/Euler#Euler_VIII
# per node:
# two 64 core cpu
# 512 GB RAM
# 920,618.0 MB Scratch

# student accounts can only use 128 GiB of RAM
# Maximum: 1024 * 128 / ntasks
