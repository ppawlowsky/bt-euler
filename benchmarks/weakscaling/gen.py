import argparse

import madupite as md
import numpy as np


def main(ranks):
    # scale both number of states and actions
    nstates = round(100000 / np.sqrt(48 / ranks))
    high = max(2, round(nstates * 0.003))
    mactions = round(100 / np.sqrt(48 / ranks))

    def transprobfunc(state, action):
        idx = np.random.choice(
            nstates, size=np.random.randint(1, high=high), replace=False
        )
        idx = np.sort(idx)
        val = np.random.uniform(low=0.0, high=1.0, size=len(idx))
        val /= val.sum()
        return idx, val

    def costfunc(state, action):
        return np.random.rand()

    md.generateMDP(
        nstates,
        mactions,
        transprobfunc,
        costfunc,
        "/cluster/scratch/ppawlowsky/mdp/transprobfixws" + str(ranks) + ".bin",
        "/cluster/scratch/ppawlowsky/mdp/stagecostfixws" + str(ranks) + ".bin",
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r",
        "--ranks",
        type=int,
        help="Number of ranks for weak scaling",
        required=True,
    )
    args = parser.parse_args()
    main(args.ranks)
