#!/bin/bash

jid=$(sbatch \
   --output=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.out \
   --error=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.err \
   --time=0:15:00 \
   --nodes=1 \
   --ntasks=1\
   --cpus-per-task=1\
   --cores-per-socket=1\
   --ntasks-per-node=1\
   --mem-per-cpu=128G \
   --wrap="mpirun python solve.py -r 1");

for i in {2..11}
do
   jid=$(sbatch \
   --output=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.out \
   --error=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.err \
   --time=3:00:00 \
   --nodes=1\
   --ntasks=1\
   --cpus-per-task=1\
   --cores-per-socket=1\
   --dependency=afterany:${jid:20:8} \
   --ntasks-per-node=1 \
   --mem-per-cpu=128G \
   --wrap="mpirun python solve.py -r $i");
done

ranks=(12 24 36 48)
for i in "${ranks[@]}"
do
   jid=$(sbatch \
   --output=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.out \
   --error=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.err \
   --time=12:00:00 \
   --nodes=1\
   --ntasks=1\
   --cpus-per-task=1\
   --cores-per-socket=1\
   --dependency=afterany:${jid:20:8} \
   --ntasks-per-node=1 \
   --mem-per-cpu=128G \
   --wrap="mpirun python solve.py -r $i");
done
# specs from https://scicomp.ethz.ch/wiki/Euler#Euler_VIII
# per node:
# two 64 core cpu
# 512 GB RAM
# 920,618.0 MB Scratch

# student accounts can only use 128 GiB of RAM and 48 cores
