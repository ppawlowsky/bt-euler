import madupite as md


def main():
    outdir = "/cluster/home/ppawlowsky/bt-euler/data/weakscalingonerank/"
        with md.PETScContextManager():
            mdp = md.PyMdp()
            for i in list(range(1, 13)) + [24, 30, 36, 42, 48]:
                mdp.loadP("/cluster/scratch/ppawlowsky/mdp/transprobws" + str(i) + ".bin")
                mdp.loadG("/cluster/scratch/ppawlowsky/mdp/stagecostws" + str(i) + ".bin")
                mdp.generateInitCost(
                    "/cluster/home/ppawlowsky/bt-euler/benchmarks/weakscaling/options.json"
                )
                for j in range(20):
                    mdp.solve(outdir + str(i) + "/" + str(j))


if __name__ == "__main__":
    main()
