#!/bin/bash

#SBATCH --output=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.out
#SBATCH --error=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.err

#SBATCH --time=1:00:00
#SBATCH --ntasks=16
#SBATCH --mem-per-cpu=8G

mpirun --report-bindings python solve.py

# specs from https://scicomp.ethz.ch/wiki/Euler#Euler_VIII
# per node:
# two 64 core cpu
# 512 GB RAM
# 920,618.0 MB Scratch

# student accounts can only use 128 GiB of RAM
