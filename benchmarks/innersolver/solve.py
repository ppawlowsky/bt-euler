import os

import madupite as md

outdir = (
    "/cluster/home/ppawlowsky/bt-euler/data/" + str(os.getenv("SLURM_JOB_ID")) + "/"
)

with md.PETScContextManager():
    mdp = md.PyMdp()
    mdp.loadP("/cluster/scratch/ppawlowsky/mdp/transprob.bin")
    mdp.loadG("/cluster/scratch/ppawlowsky/mdp/stagecost.bin")
    mdp.generateInitCost(
        "/cluster/home/ppawlowsky/bt-euler/benchmarks/innersolver/options.json"
    )

    # test performance of different inner solvers for different discount factors
    for j in range(25):
        mdp["discount factor"] = 0.02 + j * 0.04
        for i in range(10):
            mdp["solver"]["ksptype"] = "gmres"
            mdp["solver"]["preconditioner"] = "none"
            mdp.solve(outdir + "gmres" + str(j) + str(i) + "/")
            mdp["solver"]["ksptype"] = "bcgs"
            mdp.solve(outdir + "bicgstab" + str(j) + str(i) + "/")
            mdp["solver"]["ksptype"] = "richardson"
            mdp["solver"]["preconditioner"] = "jacobi"
            mdp.solve(outdir + "richard" + str(j) + str(i) + "/")
