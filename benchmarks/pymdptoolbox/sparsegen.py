"""Generate and save the forest example MDP for benchmarking PyMDPToolbox and madupite.
"""

import os
import pickle

import madupite as md
import numpy as np
from mdptoolbox.example import *


def main():
    runs = 12
    for nstates in [2 ** (i + 1) for i in range(runs)]:
        if not os.path.exists("./data/sparse/G" + str(nstates) + ".bin"):
            mactions = 2
            r1 = 4
            r2 = 2
            p = 0.1
            transprobdense, stagecostdense = forest(
                S=nstates, r1=r1, r2=r2, p=p, is_sparse=False
            )
            transprobsparse, stagecostsparse = forest(
                S=nstates, r1=r1, r2=r2, p=p, is_sparse=True
            )

            def probfunc(state, action):
                if action == 0:
                    idx, val = np.array(
                        [0, min(state + 1, nstates - 1)], dtype=np.float64
                    ), np.array([p, 1 - p])
                    return idx, val
                else:
                    idx, val = np.array([0], dtype=np.float64), np.array(
                        [1], dtype=np.float64
                    )
                    return idx, val

            # switch signs because mdptoolbox solves for rewards while
            # madupite solves for cost

            def costfunc(state, action):
                if action == 0 and state == nstates - 1:
                    return -r1
                if action == 1 and state > 0:
                    if state == nstates - 1:
                        return -r2
                    else:
                        return -1
                return 0

            md.generateMDP(
                nstates,
                mactions,
                probfunc,
                costfunc,
                "./data/sparse/P" + str(nstates) + ".bin",
                "./data/sparse/G" + str(nstates) + ".bin",
            )
            with open("./data/sparse/pydatasparseP" + str(nstates) + ".npz", "wb") as f:
                pickle.dump(transprobsparse, f)
            with open("./data/sparse/pydatasparseG" + str(nstates) + ".npz", "wb") as f:
                pickle.dump(stagecostsparse, f)
            np.savez(
                "./data/sparse/pydatadense" + str(nstates) + ".npz",
                stagecost=stagecostdense,
                transprob=transprobdense,
            )


if __name__ == "__main__":
    main()
