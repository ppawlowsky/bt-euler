"""Generate and save a random MDP for benchmarking PyMDPToolbox and madupite.
"""

import madupite as md
import numpy as np
from mdptoolbox.example import *


def main():
    with md.PETScContextManager():
        runs = 10
        for nstates in [2 ** (i + 3) for i in range(runs)]:
            if md.MPI_master():
                actions = 30
                transprobdense, _ = rand(S=nstates, A=actions, is_sparse=False)
                # pymdptoolbox returns a three dimensional stagecost that is
                # then converted to a 2d expected stage cost matrix. We sample the stagecost directly.
                stagecostdense = np.random.rand(nstates, actions)
                np.savez(
                    "./data/dense/pydata" + str(nstates) + ".npz",
                    stagecost=stagecostdense,
                    transprob=transprobdense,
                )
                # switch signs because mdptoolbox solves for rewards while
                # madupite solves for cost
                md.writePETScBinary(
                    -stagecostdense, "./data/dense/G" + str(nstates) + ".bin"
                )
                transprobdense = np.swapaxes(transprobdense, 0, 1)
                dim1, dim2, dim3 = transprobdense.shape
                transprobmat = transprobdense.reshape(dim1 * dim2, dim3)
                md.writePETScBinary(
                    transprobmat, "./data/dense/P" + str(nstates) + ".bin"
                )
                transprobdense = np.swapaxes(transprobdense, 0, 1)


if __name__ == "__main__":
    main()
