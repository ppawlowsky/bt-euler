import pickle
from collections import defaultdict

import mdptoolbox as mdpt
import numpy as np
import pandas as pd


def bellmanResidual(P, G, discount, V=None, policy=None):
    if np.array(V is None).any():
        V = np.linalg.solve(
            np.eye(P.shape[1])
            - discount * np.swapaxes(P, 0, 1)[np.arange(len(policy)), policy],
            G[np.arange(len(policy)), policy],
        )
    return np.linalg.norm(
        V
        - np.max(discount * np.einsum("ikj,j->ik", np.swapaxes(P, 0, 1), V) + G, axis=1)
    )


def main():
    timings = defaultdict(list)
    residuals = defaultdict(list)

    runs = 12
    reps = 10

    for nstates in [2 ** (i + 1) for i in range(runs)]:
        pydata = np.load("./data/sparse/pydatadense" + str(nstates) + ".npz")
        stagecostdense = pydata["stagecost"]
        transprobdense = pydata["transprob"]
        discount = 0.9999
        with open("./data/sparse/pydatasparseP" + str(nstates) + ".npz", "rb") as f:
            transprobsparse = pickle.load(f)
        with open("./data/sparse/pydatasparseG" + str(nstates) + ".npz", "rb") as f:
            stagecostsparse = pickle.load(f)

        for solver, name in zip(
            [
                mdpt.mdp.PolicyIteration,
                mdpt.mdp.PolicyIterationModified,
                mdpt.mdp.ValueIteration,
                # mdpt.mdp.ValueIterationGS,
                mdpt.mdp.PolicyIteration,
                mdpt.mdp.PolicyIterationModified,
                mdpt.mdp.ValueIteration,
                # mdpt.mdp.ValueIterationGS,
            ],
            [
                "PI",
                "PIModified",
                "VI",
                # "GS", no convergence
                "PIsparse",
                "PIModifiedsparse",
                "VIsparse",
                # "GSsparse" no convergence
            ],
        ):
            for i in range(reps):
                if name == "PIModified":
                    mdptoolbox = solver(transprobdense, stagecostdense, discount)
                elif name == "VI" or name == "GS":
                    mdptoolbox = solver(transprobdense, stagecostdense, discount)
                elif name == "PI":
                    mdptoolbox = solver(transprobdense, stagecostdense, discount)
                elif name == "PIModifiedsparse":
                    mdptoolbox = solver(transprobsparse, stagecostsparse, discount)
                elif name == "VIsparse":
                    mdptoolbox = solver(transprobsparse, stagecostsparse, discount)
                elif name == "GSsparse":
                    if nstates <= 256:
                        mdptoolbox = solver(transprobsparse, stagecostsparse, discount)
                    else:
                        timings[name].append(-1)
                        residuals[name].append(-1)
                elif name == "PIsparse":
                    mdptoolbox = solver(transprobsparse, stagecostsparse, discount)
                if name == "GSsparse" and nstates > 256:
                    pass
                else:
                    mdptoolbox.run()
                    timings[name].append(mdptoolbox.time)
                    if name == "PI" or name == "PIsparse":
                        residuals[name].append(
                            bellmanResidual(
                                transprobdense,
                                stagecostdense,
                                discount,
                                mdptoolbox.V,
                            )
                        )
                    else:
                        residuals[name].append(
                            bellmanResidual(
                                transprobdense,
                                stagecostdense,
                                discount,
                                policy=mdptoolbox.policy,
                            )
                        )
    df = pd.DataFrame.from_dict(timings)
    df.to_csv("sparsetimingstoolbox.csv")
    df = pd.DataFrame.from_dict(residuals)
    df.to_csv("sparseresidualstoolbox.csv")


if __name__ == "__main__":
    main()
