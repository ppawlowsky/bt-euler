from collections import defaultdict

import madupite as md
import numpy as np
import pandas as pd


def bellmanResidual(P, G, discount, V=None, policy=None):
    if np.array(V is None).any():
        V = np.linalg.solve(
            np.eye(P.shape[1])
            - discount * np.swapaxes(P, 0, 1)[np.arange(len(policy)), policy],
            G[np.arange(len(policy)), policy],
        )
    return np.linalg.norm(
        V
        - np.max(discount * np.einsum("ikj,j->ik", np.swapaxes(P, 0, 1), V) + G, axis=1)
    )


def main():
    with md.PETScContextManager():
        timings = defaultdict(list)
        residuals = defaultdict(list)
        runs = 10
        reps = 10
        for nstates in [2 ** (i + 3) for i in range(runs)]:
            pydata = np.load("./data/dense/pydata" + str(nstates) + ".npz")
            stagecostdense = pydata["stagecost"]
            transprobdense = pydata["transprob"]
            discount = 0.9999
            mdp = md.PyMdp()
            mdp.loadP("./data/dense/P" + str(nstates) + ".bin")
            mdp.loadG("./data/dense/G" + str(nstates) + ".bin")
            mdp.generateInitCost("./options.json")
            mdp["discount factor"] = discount
            mdp["solver"]["inner stopping criterion"]["alpha"] = "default"
            for i in range(reps):
                timings["madupite"].append(mdp.solve("./", ""))
                if md.MPI_master():
                    madupitev = np.loadtxt("optcost.txt", delimiter=",", skiprows=2)
                    residuals["madupite"].append(
                        bellmanResidual(
                            transprobdense,
                            stagecostdense,
                            discount,
                            -madupitev,
                        )
                    )
    df = pd.DataFrame.from_dict(timings)
    df.to_csv("densetimingsmadupite.csv")
    df = pd.DataFrame.from_dict(residuals)
    df.to_csv("denseresidualsmadupite.csv")


if __name__ == "__main__":
    main()
