#pragma once

#include "json.hpp"
using json = nlohmann::json;
#include <petsc.h>
#include <string>

class Mdp {
public:
  // Load P from a file
  void loadP(std::string filename);

  // Load G from a file
  void loadG(std::string filename);

  // Generate random initial cost
  void generateInitCost(std::string injson = "");

  // given a policy extract the associated transition probability matrix
  void extractTransProbMat(PetscInt *policy);

  // given a policy extract the associated stage-cost vector
  void extractCostVec(PetscInt *policy);

  // given a cost-to-go estimate in optcost, compute the greedy policy,
  // the residual and log the data in logjson.
  void greedyPolicy(PetscInt *policy, PetscReal *residual, json *logjson);

  // given policy and discount gamma, solve for corresponding cost
  void solveCost(json *logjson, PetscReal *residual);

  // Solve according to solver type
  double solve(std::string outdir, std::string injson = "");

  // constructors
  Mdp();

  // destructor
  ~Mdp();

  Mat P;                // transition probability tensor wrapped in matrix
  Mat G;                // stage cost matrix
  Mat P_policy;         // cur transprob for policy
  Vec optcost;          // optimal cost vector
  Vec initcost;         // initial cost vector
  Vec g_policy;         // policy vector
  PetscInt nstates;     // number of states
  PetscInt mactions;    // number of actions
  PetscInt localStates; // number of states on this rank
  PetscInt maxnnzperrow;
  PetscInt rank; // rank
  PetscInt size; // number of ranks
  json data;     // json data
  PetscBool useShell = PETSC_FALSE;

  // user options
  PetscReal discount;
  PetscReal alpha;
  PetscReal solve_start_time;
  PetscReal outer_tolerance; // tolerance for the Bellman residual function
};
