#include <cstdlib>
#include <cstring>
#include <experimental/filesystem>
#include <iostream>
#include <string>

#include "include/mdp.hpp"

int main(int argc, char **argv) {
  // create directory to store output files
  std::string outfilepath;
  std::string env_p = std::getenv("SLURM_JOB_ID");
  outfilepath = "/cluster/home/ppawlowsky/bt/data/" + env_p + "/";
  std::experimental::filesystem::current_path("/");
  std::experimental::filesystem::create_directories(outfilepath);
#if PETSC_VERSION_LT(3, 19, 0)
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
#else
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);
#endif
  // PetscLogDefaultBegin();

  Mdp mdp;
  double start, loadP, loadG, init, solve;
  start = MPI_Wtime();
  mdp.loadP("/cluster/scratch/ppawlowsky/mdp/transprob.bin");
  loadP = MPI_Wtime();
  mdp.loadG("/cluster/scratch/ppawlowsky/mdp/stagecost.bin");
  loadG = MPI_Wtime();
  mdp.generateInitCost("/cluster/home/ppawlowsky/bt/options.json");
  init = MPI_Wtime();
  for (size_t i = 0; i < 20; i++) {
    mdp.solve(outfilepath + std::to_string(i), "/cluster/home/ppawlowsky/bt/options.json");
  }

  solve = MPI_Wtime();
  PetscPrintf(PETSC_COMM_WORLD, "Time:\nloadP,%f\nloadG,%f\nsolve,%f\n", loadP - start,
              loadG - loadP, solve - init);
  mdp.~Mdp();

  // write logview to specific destination
  // This is not possible with the options database because
  // the job id is not available in a sbatch --wrap
  PetscViewer viewer;
  PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
  PetscViewerSetType(viewer, PETSCVIEWERASCII);
  PetscViewerPushFormat(viewer, PETSC_VIEWER_ASCII_SYMMODU);
  PetscViewerFileSetMode(viewer, FILE_MODE_WRITE);
  PetscViewerFileSetName(viewer, (outfilepath + "/log.txt").c_str());
  PetscLogView(viewer);
  PetscViewerDestroy(&viewer);
  PetscFinalize();
  return 0;
}
